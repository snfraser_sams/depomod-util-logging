package com.srsl.util.logging;

/**
 * Collects information necessary to generate LogRecords. LogCollators are created by
 * a LogGenerator by calling LogGenerator.create() - this creates a new collator instance and 
 * a new LogRecord instance which is reused after a call is made to send(). The send() method
 * outputs the LogRecord to any handlers attached to the base Logger. The various setter methods in
 * LogCollator all return the instance on which they were called so as to allow chaining 
 * while a record is being built up. The system/subsystem/source/id fields are left untouched after
 * a call to send() as they are expected to apply to the current instance of the class in which the
 * collator send() has been called. All other mutable fields are reset and the LogRecord re-used.
 * 
 *  If a collator is likely to be called from several threads then it should be regenerated for each call
 *  
 *  e.g. g.create().thread().msg().send() creates a new collator from generator g.
 * 
 * Usage example:-
 *
 * <code>
 *  Logger l = LogManager.getLogger("TEST");
 *  LogGenerator g = logger.generate().system("X").subSystem("Y").source(this.getClass().getName()).id(this.hashcode());
 *  
 *  // all the top level Identities are now fixed for this generator and passed to the collator when we call create() 
 *  LogCollator log = g.create();
 *  
 *  log.warning().level(2).msg("A test").
 *    context("K1", "V1").
 *    context("K2", "V2").
 *    context("K3", "V3").
 *    tag("INFO).
 *     send();
 * </code>
 * 
 *  When we call send() all the lower level params are reset but system params stay fixed in the collator.
 *  
 *  If it is required to call the same collator from multiple threads it is advisable to renew the collator each call
 *  by calling generator.create() which will repopulate the system level fields.
 *  
 *  <code>
 *   g.create().level(2).thread().msg("A test from Thread#1").send(); // called from Thread#1
 *   g.create().level(1).thread().msg("A test from Thread#2").send(); // called from Thread#2
 *  </code>
 * 
 *  Both the above examples will have the system/subsystem/source/id fields correctly populated.
 *  
 *  Condition: A potential new feature to allow very fine level control on log messages, 
 *  use sparingly as the expression is evaluated each time the log record is created and 
 *  only bales out at the moment of sending. COULD BE ISSUES WITH SHARING A COLLATOR???
 *  
 *  e.g. log a message if handling a specfic cell in a 2D array
 *  int i0 = 5; j0 = 4; imax = 10; jmax = 10;
 *  for (int i = 0; i < imax; i++) {
 *    for (int j = 0; j < jmax; j++) {
 *       logger.info().level(3).condition(i == i0 && j == j0).msg("Processing special cell").send();
 *       // do stuff on each cell
 *    }
 *  }
 *   That message will only be logged if the cell indices are: (i,j) = (i0,j0)
 */
public class LogCollator {

	/** A Logger to send the generated ELR via. */
	private Logger logger;

	/** An ELR to build. */
	private LogRecord record;
	
	/**
	 * Create a LogCollator bound to the specified Logger.
	 * 
	 * @param logger A logger to send the collated ELR log via.
	 */
	public LogCollator(Logger logger) {
		this.logger = logger;
		record = new LogRecord(1, logger.getName());
		// Set some default values in case they are not set.
		record.setLevel(1);
		record.setSeverity(Logging.SEVERITY_INFORMATION);
	}

	/**
	 * Set the ELR system field.
	 * 
	 * @param system the value of the system field.
	 * @return This LogCollator.
	 */
	public LogCollator system(String system) {
		record.setSystem(system);
		return this;
	}

	/**
	 * Set the ELR sub system field.
	 * 
	 * @param subSystem the value of the sub-system field.
	 * @return This LogCollator.
	 */
	public LogCollator subSystem(String subSystem) {
		record.setSubSystem(subSystem);
		return this;
	}

	/**
	 * Set the ELR source component class field.
	 * 
	 * @param srcCompClass the value of the source component class field.
	 * @return This LogCollator.
	 */
	public LogCollator source(String srcCompClass) {
		record.setSource(srcCompClass);
		return this;
	}

	/**
	 * Set the ELR source component ID field.
	 * 
	 * @param srcCompId the value of the source component ID field.
	 * @return This LogCollator.
	 */
	public LogCollator id(String srcCompId) {
		record.setId(srcCompId);
		return this;
	}

	/**
	 * Set the ELR source block ID field.
	 * 
	 * @param block the value of the source block ID field.
	 * @return This LogCollator.
	 */
	public LogCollator block(String block) {
		record.setBlock(block);
		return this;
	}

	/**
	 * Set the ELR severity field.
	 * 
	 * @param severity the value of the severity field.
	 * @return This LogCollator.
	 */
	public LogCollator severity(int severity) {
		record.setSeverity(severity);
		return this;
	}

	/**
	 * Set the ELR level field.
	 * 
	 * @param level the value of the level field.
	 * @return This LogCollator.
	 */
	public LogCollator level(int level) {
		record.setLevel(level);
		return this;
	}

	/**
	 * Set the ELR message field.
	 * 
	 * @param message the value of the message field.
	 * @return This LogCollator.
	 */
	public LogCollator msg(String message) {
		record.setMessage(message);
		return this;
	}
	
	/** Set the ELR message field according to the specified format and values.
	 * 
	 * @param format The format specifier.
	 * @param values A set of values to format using the format specifier.
	 * @return This logCollator.
	 */
	public LogCollator fmt(String format, Object... values) {
		record.setMessage(String.format(format, values));
		return this;
	} 

	/**
	 * Add a context entry (key, value) pair.
	 * 
	 * @param key   the context variable name.
	 * @param value the context variable value.
	 * @return This LogCollator.
	 */
	public LogCollator context(String key, String value) {
		record.addContext(key, value);
		return this;
	}

	/**
	 * Add a tag entry.
	 * 
	 * @param tag the tag.
	 * @return This LogCollator.
	 */
	public LogCollator tag(String tag) {
		record.addTag(tag);
		return this;
	}

	/**
	 * Convenience method. Sets the ELR severity level to FATAL.
	 * 
	 * @return This LogCollator.
	 */
	public LogCollator fatal() {
		record.setSeverity(Logging.SEVERITY_FATAL);
		return this;
	}

	/**
	 * Convenience method. Sets the ELR severity level to CRITICAL.
	 * 
	 * @return This LogCollator.
	 */
	public LogCollator critical() {
		record.setSeverity(Logging.SEVERITY_CRITICAL);
		return this;
	}

	/**
	 * Convenience method. Sets the ELR severity level to ERROR.
	 * 
	 * @return This LogCollator.
	 */
	public LogCollator error() {
		record.setSeverity(Logging.SEVERITY_ERROR);
		return this;
	}

	/**
	 * Convenience method. Sets the ELR severity level to WARNING.
	 * 
	 * @return This LogCollator.
	 */
	public LogCollator warn() {
		record.setSeverity(Logging.SEVERITY_WARNING);
		return this;
	}

	/**
	 * Convenience method. Sets the ELR severity level to INFORMATION.
	 * 
	 * @return This LogCollator.
	 */
	public LogCollator info() {
		record.setSeverity(Logging.SEVERITY_INFORMATION);
		return this;
	}

	/** Set the logging level to FINE (5).*/
	public LogCollator fine() {
		record.setLevel(Logging.LEVEL_FINE);
		return this;
	} 
	
	/** Set the logging level to TRACE (4).*/
	public LogCollator trace() {
		record.setLevel(Logging.LEVEL_TRACE);
		return this;
	}
	
	
	
	/** Set the logging level to DEBUG (3).*/
	public LogCollator debug() {
		record.setLevel(Logging.LEVEL_DEBUG);
		return this;
	} 
	
	/** Set the logging level to DETAIL (2).*/
	public LogCollator detail() {
		record.setLevel(Logging.LEVEL_DETAIL);
		return this;
	} 
	
	/** Set the logging level to SUMMARY (1).*/
	public LogCollator summary() {
		record.setLevel(Logging.LEVEL_SUMMARY);
		return this;
	} 
	
	public LogCollator thread() {
		record.setThread(Thread.currentThread().getName());
		return this;
	}
	
	/**
	 * Set the record's enablement condition.
	 * Acts as a dynamic filter: e.g. collator.condition(x == 1 && y == 2).send()
	 * @param condition The condition status.
	 * @return
	 */
	public LogCollator condition(boolean condition) {
		record.setCondition(condition);
		return this;
	}
	
	/** This method renews the contained record.
	 *  NOTE: it resets all the system fields blank as these are usually set by
	 *  LogGenerator.create() which passes the system field values. 
	 *  These values are not retained by the collator.
	 *  Note: An attempt has been made to rectify this, needs further testing.
	 *  */
	public LogCollator reset() {
		String system = record.getSystem();
		String subSystem = record.getSubSystem();
		String source = record.getSource();
		String id = record.getId();
		// copy the old system fields
		record = new LogRecord(1, logger.getName());
		record.setSystem(system);
		record.setSubSystem(subSystem);
		record.setSource(source);
		record.setId(id);
		return this;
	}
	
	/** Publish the log record (ELR) to the attached logger. */
	public void send() {
		record.setTime(System.currentTimeMillis());
	
		logger.log(record);
	
		// clear out any fields we may fail to override next call...
		record.clearTags();
		record.clearContext();
		record.setBlock(null);
		record.setThread(null);
		record.setException(null);
		record.setSeverity(Logging.SEVERITY_INFORMATION);
		record.setLevel(5);
	}

	/**
	 * Sets various fields based on call info extracted from stack frames - very
	 * expensive.
	 */
	public LogCollator extractCallInfo() {
		

		// save any state we may want to keep 
		// - these need to be visible after we reset them with extract...
		// - so they can be reset next time we call send without having reset them explicitly!!
		String source = record.getSource();
		String subsystem = record.getSubSystem();
		String system = record.getSystem();
		
		
		Throwable t = new Throwable();
		StackTraceElement[] stack = t.getStackTrace();

		// System.err.println("Stack [0]= "+stack[0]);

		if (stack.length > 1) {
			record.setSource(stack[1].getClassName());
			record.setBlock(stack[1].getMethodName() + ":" + stack[1].getLineNumber());
			// System.err.println("Called by: "+stack[1]);
			record.setThread(Thread.currentThread().getName());
		}
		return this;
	}

	public String toString() {
		return "LogCollator: Bound to: " + logger.getName() + ", ELR=[" + record + "]";
	}

}
