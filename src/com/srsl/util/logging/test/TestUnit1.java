package com.srsl.util.logging.test;

import java.util.UUID;

import com.srsl.util.logging.LogCollator;
import com.srsl.util.logging.LogGenerator;
import com.srsl.util.logging.LogManager;
import com.srsl.util.logging.Logger;

public class TestUnit1 {

	private String name;

	private String system;

	private String subSystem;

	private LogCollator logger;

	/**
	 * @param name
	 */
	public TestUnit1(String name) {
		this.name = name;

	}

	/**
	 * @param name
	 * @param system
	 * @param subSystem
	 */
	public TestUnit1(String name, String system, String subSystem) {
		this.name = name;
		this.system = system;
		this.subSystem = subSystem;

		UUID uid = UUID.randomUUID();
		String suid = uid.toString().substring(0, 8);
		Logger local = LogManager.getLogger(name);
		LogGenerator gen = local.generate().system(system).subSystem(subSystem).source(this.getClass().getSimpleName()).id(suid);

		logger = gen.create();

	}

	public void exec(int block) {

		switch (block) {
		case 1:
			logger.warn().level(2).thread().msg("Enter block with param " + block).send();
			break;
		case 2:
			logger.info().level(3).thread().msg("Running current block").send();
			break;
		case 3:
			double result = Math.random()*1000.0;
			logger.info().level(2).thread().msg("Completed with result: " + result).send();
			break;
		default:
			logger.error().level(1).thread().msg("Invalid block value: "+block).send();
			break;

		}

	}

}
