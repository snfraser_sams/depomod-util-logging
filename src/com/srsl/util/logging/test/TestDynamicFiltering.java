package com.srsl.util.logging.test;

import java.util.UUID;

import com.srsl.util.logging.LogCollator;
import com.srsl.util.logging.LogGenerator;
import com.srsl.util.logging.LogManager;
import com.srsl.util.logging.Logger;
import com.srsl.util.logging.formatters.DefaultLogFormatter;
import com.srsl.util.logging.handlers.ConsoleLogHandler;

public class TestDynamicFiltering {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Logger l = LogManager.getLogger("TEST");
		ConsoleLogHandler console = new ConsoleLogHandler(new DefaultLogFormatter());
		console.setLogLevel(5);
		l.addHandler(console);
		l.setLogLevel(3);
		
		LogGenerator local = l.generate().system("Model").subSystem("Transport").source("CartesianSuspension");
		
		
		LogCollator logger = local.create();
	
		for (int i = 0; i < 1000; i++) {
			
			UUID uuid = UUID.randomUUID();
			String suid = uuid.toString().substring(0, 8);
			
			//logger.info().level(2).source("Transports.Suspension").id(suid).msg("This is cartesian suspension index "+i).send();
			logger.warn().level(2).source("Bed.Iterator").thread().id(suid).condition(i % 5 == 0).msg("This is model iter index "+i).send();
			logger.info().level(2).source("Sus.Iterator").thread().id(suid).condition(i % 2 == 0).msg("This is other iter index "+i).send();
		
			try {Thread.sleep(300L);} catch (InterruptedException ix) {}
		}
		
		
	}

}
