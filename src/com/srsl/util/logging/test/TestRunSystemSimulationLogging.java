/**
 * 
 */
package com.srsl.util.logging.test;

import com.srsl.util.logging.LogManager;
import com.srsl.util.logging.Logger;
import com.srsl.util.logging.formatters.DefaultLogFormatter;
import com.srsl.util.logging.handlers.ConsoleLogHandler;

/**
 * Simulate a whole (simple) system running and logging.
 * 
 * @author SA05SF
 *
 */
public class TestRunSystemSimulationLogging {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// create a bunch of loggers...
		ConsoleLogHandler console = new ConsoleLogHandler(new DefaultLogFormatter());
		console.setLogLevel(5);
		
		Logger lm = LogManager.getLogger("MODEL");
		lm.addHandler(console);
		lm.setLogLevel(5);

		Logger la = LogManager.getLogger("AUTH");	
		la.addHandler(console);
		la.setLogLevel(5);
		
	}
	
	private class Auth {
		
		public void login() {
		
			// read user details etc
			
			
		}
		
		public void checkLicense() {}
		
		public void checkFarms() {}
		
	}
	
	private class Model extends Thread {
		
		public void run() {
			
			Auth auth = new Auth();
			auth.login();
			
			
			
			
			
			
		}
		
	}
	
	private class Init {
		
		public void init() {}
		
	}
	
	private class Output {
		
		public void genSurfaces() {}
		
		public void genSnapshot() {}
		
		public void genSeries() {}
		
	}
	
	private class Setup {
		
		public void setupModel() {}
		
	}

}
