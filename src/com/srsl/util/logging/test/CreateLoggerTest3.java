package com.srsl.util.logging.test;

import java.util.UUID;

import com.srsl.util.logging.LogCollator;
import com.srsl.util.logging.LogGenerator;
import com.srsl.util.logging.LogManager;
import com.srsl.util.logging.Logger;
import com.srsl.util.logging.formatters.BasicLogFormatter;
import com.srsl.util.logging.formatters.BogstanLogFormatter;
import com.srsl.util.logging.formatters.DefaultLogFormatter;
import com.srsl.util.logging.formatters.TabbedLogFormatter;
import com.srsl.util.logging.handlers.ConsoleLogHandler;

public class CreateLoggerTest3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//TabbedLogFormatter.setupTabs(3, '.');
		
		Logger l = LogManager.getLogger("TEST");
		TabbedLogFormatter tlf1 = new TabbedLogFormatter(3, '.');
		tlf1.setPrefixTime(true);
		ConsoleLogHandler console = new ConsoleLogHandler(tlf1);
		console.setLogLevel(5);
		l.addHandler(console);
		l.setLogLevel(5);
		
		Logger l2 = LogManager.getLogger("SYS");
		TabbedLogFormatter tlf2 = new TabbedLogFormatter(3, '.');
		tlf2.setPrefixTime(true);
		ConsoleLogHandler console2 = new ConsoleLogHandler(tlf2);
		console2.setLogLevel(5);
		l2.addHandler(console2);
		l2.setLogLevel(5);
		
		
		
		LogGenerator local = l.generate().system("Model").subSystem("Transport").source("CartesianSuspension");
		LogGenerator local2 = l2.generate().system("Model").subSystem("Numerics").source("Calc");
		
		
		LogCollator logger = local.create();
		LogCollator logger2 = local2.create(); 
		
		for (int i = 0; i < 1000; i++) {
			
			UUID uuid = UUID.randomUUID();
			String suid = uuid.toString().substring(0, 8);
			
			logger.info().level(1+i%5).id(suid).tag((i % 3 == 0 ? "DEBUG":"AHA")).tag("B6").tag("JK87").msg("This is debug index "+i).send();
			logger.warn().level(1+i%5).thread().id(suid).tag((i % 3 == 0 ? "TRACE":"")).msg("This is also debug index "+i).send();
			
			logger2.info().level(2).tag("TEST").tag("ERROR").msg("A message with some tags appended to it by the formatter").send();
			
			logger2.fatal().level(1).msg("Aaaaargh an error has occurred").send();
			logger2.extractCallInfo().level(3).tag("EXTRA").msg("Here is some extra information").send();
			logger2.extractCallInfo().level(4).tag("EXTRA_EXTRA").msg("Here is some detailed extra information").send();
			logger2.extractCallInfo().level(5).thread().id(suid).tag("EXTRA_ULTRA").msg("Here is some seriously detailed extra information").send();
			
			try {Thread.sleep(1000L);} catch (InterruptedException ix) {}
		}
		

	}

}
