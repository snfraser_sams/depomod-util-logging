package com.srsl.util.logging.test;

import java.util.UUID;

import com.srsl.util.logging.LogCollator;
import com.srsl.util.logging.LogGenerator;
import com.srsl.util.logging.LogManager;
import com.srsl.util.logging.Logger;
import com.srsl.util.logging.filters.TagLogFilter;
import com.srsl.util.logging.formatters.BasicLogFormatter;
import com.srsl.util.logging.formatters.BogstanLogFormatter;
import com.srsl.util.logging.formatters.DefaultLogFormatter;
import com.srsl.util.logging.handlers.ConsoleLogHandler;

public class CreateLoggerTest1 {

	public static void main(String[] args) {
	
		Logger l = LogManager.getLogger("TEST");
		ConsoleLogHandler console = new ConsoleLogHandler(new DefaultLogFormatter());
		console.setLogLevel(5);
		l.addHandler(console);
		l.setLogLevel(3);
		
		TagLogFilter f1 = new TagLogFilter(true);
		f1.setUseSourceClassAsTag(true);
		f1.addTag("Model");
		l.setFilter(f1);
		
		
		Logger l2 = LogManager.getLogger("SYS");
		ConsoleLogHandler console2 = new ConsoleLogHandler(new DefaultLogFormatter());
		console2.setLogLevel(5);
		l2.addHandler(console2);
		l2.setLogLevel(3);
		
		TagLogFilter f2 = new TagLogFilter(true);
		f2.setUseSourceClassAsTag(true);
		f2.addTag("Transport");
		f2.addTag("Model");
		l2.setFilter(f2);
		
		LogGenerator local = l.generate().system("Model").subSystem("Transport").source("CartesianSuspension");
		LogGenerator local2 = l2.generate().system("Model").subSystem("Numerics").source("Calc");
		
		
		LogCollator logger = local.create();
		LogCollator logger2 = local2.create(); 
		
		for (int i = 0; i < 1000; i++) {
			
			double a = Math.random();
			double b = Math.random();
			
			UUID uuid = UUID.randomUUID();
			String suid = uuid.toString().substring(0, 8);
			
			logger.info().level(i%5).source("Transports.Suspension").id(suid).tag((i % 3 == 0 ? "DEBUG":"AHA")).tag("B6").tag("JK87").msg("This is cartesian suspension index "+i).send();
			logger.warn().level(i%5).source("Model.Iterator").thread().id(suid).tag((i % 3 == 0 ? "TRACE":"")).msg("This is model iter index "+i).send();
			
			logger2.info().level(2).source("Model.Runtime").tag("TEST").tag("ERROR").msg("A message with some tags appended to it by the formatter").send();
			
			logger2.info().level(2).source("Model.Runtime").tag("LIVE").tag("FMTDATA").fmt("Fmt-data:: a=%f, b=%f i=%d", a, b, i).send();
			
			logger2.fatal().level(1).source("Transport.Controller").thread().msg("Aaaaargh an error has occurred").send();
			
			try {Thread.sleep(30L);} catch (InterruptedException ix) {}
		}
		

	}

}
