/**
 * 
 */
package com.srsl.util.logging.test;

import java.util.UUID;

import com.srsl.util.logging.LogGenerator;
import com.srsl.util.logging.LogManager;
import com.srsl.util.logging.Logger;
import com.srsl.util.logging.formatters.DefaultLogFormatter;
import com.srsl.util.logging.formatters.TabbedLogFormatter;
import com.srsl.util.logging.handlers.ConsoleLogHandler;

/**
 * Creates 2 threads to make simultaneous calls on the same class
 * 
 * @author SA05SF
 *
 */
public class CreateLogger2ThreadsTest1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// setup logging...
		Logger l = LogManager.getLogger("TASK");
		ConsoleLogHandler console = new ConsoleLogHandler(new DefaultLogFormatter());
		console.setLogLevel(5);
		l.addHandler(console);
		l.setLogLevel(5);

		Logger l2 = LogManager.getLogger("AUTH");
		l2.addHandler(console);
		l2.setLogLevel(5);

		Thread t1 = new Thread(new MyThread1("T1.A", false));
		t1.setName("Thread-a");
		Thread t2 = new Thread(new MyThread1("T2.B", false));
		t2.setName("Thread-b");
		Thread t3 = new Thread(new MyThread1("T3.C", false));
		t3.setName("Thread-c");

		Thread t4 = new Thread(new MyThread1("Comms", true));
		t4.setName("SysAuth");

		t1.start();
		t2.start();
		t3.start();
		t4.start();

	}

	public static class MyThread1 implements Runnable {

		private LogGenerator g;
		private String mytag;
		private boolean auth;

		String uid = UUID.randomUUID().toString().substring(0, 8);

		public MyThread1(String mytag, boolean auth) {
			this.mytag = mytag;
			this.auth = auth;

			Logger l = null;
			if (auth) {
				l = LogManager.getLogger("AUTH");
				g = l.generate().system("Auth").subSystem("Login").source("Lcheck").id(uid);
			} else {
				l = LogManager.getLogger("TASK");
				g = l.generate().system("Model").subSystem("CartSusp").source("CsImp").id(uid);
			}
			// we have a generator so can call g.create()...to create separate collators
		}

		public void run() {

			try {
				long init = (long) (Math.random() * 2000.0 + 1000.0);
				Thread.sleep(init);
			} catch (InterruptedException ix) {
			}
			for (int i = 0; i < 1000; i++) {

				if (Math.random() < 0.3) {
					g.create().info().level(i % 6 + 1).thread().msg("And this is a message from a thread").tag(mytag)
							.tag("init").send();
				} else if (Math.random() < 0.95) {
					g.create().warn().level(i % 6 + 1).thread().msg("And this is a message from a thread").tag(mytag)
							.tag("new").send();
				} else {
					g.create().critical().level(i % 6 + 1).thread().msg("And this is a message from a thread")
							.tag(mytag).tag("v1.4").send();
				}

				if (auth) {
					g.create().debug().level(2).thread().msg("Debug message from authentication subsystem")
							.tag(mytag).tag("debug").tag("auth").tag("system").send();
				}

			

			try {
				Thread.sleep(1200L);
			} catch (InterruptedException ix) {
			}
			}
		}

	}

}
