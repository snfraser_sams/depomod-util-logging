package com.srsl.util.logging.test;

import com.srsl.util.logging.LogManager;
import com.srsl.util.logging.Logger;
import com.srsl.util.logging.formatters.DefaultLogFormatter;
import com.srsl.util.logging.handlers.ConsoleLogHandler;

public class CreateTestUnit1 {

	public static void main(String[] args) {
	
		Logger l = LogManager.getLogger("TEST");
		ConsoleLogHandler console = new ConsoleLogHandler(new DefaultLogFormatter());
		console.setLogLevel(5);
		l.addHandler(console);
		l.setLogLevel(5);
		
		TestUnit1 tu1 = new TestUnit1("TEST", "Model", "Numerics");
		TestUnit1 tu2 = new TestUnit1("TEST", "Model", "Transport");
		TestUnit1 tu3 = new TestUnit1("TEST", "Results", "Surfaces");
		
		for (int i = 0; i < 1000; i++) {
			int k = (int)(Math.random()*4);
			tu1.exec(k);
			int p = (int)(Math.random()*4);
			tu2.exec(p);
			int m = (int)(Math.random()*4);
			tu3.exec(m);
			
			try {Thread.sleep(100L);} catch (InterruptedException ix) {} 
		}
		
	}

}
