package com.srsl.util.logging.test;

import java.util.UUID;

import com.srsl.util.logging.LogCollator;
import com.srsl.util.logging.LogGenerator;
import com.srsl.util.logging.LogManager;
import com.srsl.util.logging.Logger;
import com.srsl.util.logging.Logging;
import com.srsl.util.logging.filters.TagLogFilter;
import com.srsl.util.logging.formatters.BasicLogFormatter;
import com.srsl.util.logging.formatters.BogstanLogFormatter;
import com.srsl.util.logging.formatters.DefaultLogFormatter;
import com.srsl.util.logging.handlers.ConsoleLogHandler;

public class CreateLoggerTest2 {

	public static void main(String[] args) {

		TagLogFilter tlf = new TagLogFilter(true);
		tlf.addTag("TEST");
		tlf.addTag("TRACE");
		tlf.addTag("BATHY");
		tlf.addTag("LIFECYCLE");
		
		Logger l = LogManager.getLogger("TEST");
		ConsoleLogHandler console = new ConsoleLogHandler(new DefaultLogFormatter());
		console.setLogLevel(5);
		l.addHandler(console);
		l.setLogLevel(5);
		l.setFilter(tlf);

		Logger l2 = LogManager.getLogger("SYS");
		ConsoleLogHandler console2 = new ConsoleLogHandler(new DefaultLogFormatter());
		console2.setLogLevel(5);
		l2.addHandler(console2);
		l2.setLogLevel(5);
		l2.setFilter(tlf);
		
		
		MyThread t1 = new MyThread("MT1");
		MyThread t2 = new MyThread("MT2");
		MyThread t3 = new MyThread("MT3");
		
		(new Thread(t1)).start();
		(new Thread(t2)).start();
		(new Thread(t3)).start();
	}

	public static class LogTester {

		Logger l;
		Logger l2;
		LogCollator logger;
		LogCollator logger2; 
		
		public void create() {
			l = LogManager.getLogger("TEST");
			l2 = LogManager.getLogger("SYS");
		
			LogGenerator local = l.generate().system("Model").subSystem("Transport").source("CartesianSuspension");
			LogGenerator local2 = l2.generate().system("Model").subSystem("Numerics").source("Calc");
			
			
			 logger = local.create();
			 logger2 = local2.create(); 
			
		}

		public void runtest(int i) {

			UUID uuid = UUID.randomUUID();
			String suid = uuid.toString().substring(0, 8);
			
			int sev = (int)(Math.random()*5.0 + 1.0);
			
			logger.severity(sev).level(4).thread().id(suid).tag((i % 3 == 0 ? "BATHY.XY":"FLOW")).tag("MESH").tag("TRANS").msg("This is debug index "+i).send();
			logger.warn().level(i%5).thread().id(suid).tag((i % 3 == 0 ? "TRACE":"BATHY.DEPTH")).msg("This is mesh search index "+i).send();
			
			logger2.info().detail().tag("NETWORK").tag((i % 2 == 0 ? "LIFECYCLE":"BATHY.NET")).fmt(" I=%d I2=%d",i, i*i).send();
			logger2.severity(sev).trace().level(1+i%5).msg("An unspecified event of some sort has occurred").send();
			logger2.debug().msg("A debug level message with default severity").send();
			
		}

	}

	public static class MyThread implements Runnable {

		String name;

		MyThread(String name) {
			this.name = name;
		}

		@Override
		public void run() {
			
			LogTester lt = new LogTester();
			lt.create();
			
			for (int i=0; i < 1000; i++) {
				
				lt.runtest(i);
				
				long dt = (long)(Math.random()*50.0);
				try { Thread.sleep(50L + dt);} catch (InterruptedException ix) {}
				
			}
		

		}

	}

}
