package com.srsl.util.logging;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** Stores a mesage and associated information from a Logging call. */
public class LogRecord implements Serializable {

	/**
	 * Serial version UID - used to maintain serialization compatibility across
	 * modifications of the class's structure.
	 */
	private static final long serialVersionUID = -8299743037667027751L;

	/** The system which generated the log message. */
	private String system;

	/** The sub-system/module which generated the log message. */
	private String subSystem;

	/**
	 * The message placed in the original log call. This may contain information in
	 * a format which can be parsed by a LogFormatter.
	 */
	protected String message;

	/** The logger which originated this LogRecord. */
	protected String loggerName;

	/** The identity of the class which contained the log call. */
	protected String source;

	/** The identity of the object which contained the log call. */
	protected String id;

	/**
	 * The method or block within the originating object where the call was made.
	 */
	protected String block;

	/** The thread which made the log call. */
	protected String thread;

	/**
	 * Severity (in terms of effect on the system) of the event.
	 */
	private volatile int severity;

	/** The time the log call was made. */
	protected long time;

	/** The unique (to the calling Logger) sequence no of this message. */
	protected volatile int seqno;

	/** The level (granularity) of the message. */
	protected volatile int level;

	/**
	 * Mapping from keyword to value for various relevant environment (context)
	 * variables.
	 */
	private Map context;

	/** A set of tags to add to the record. */
	private List<String> tags;

	/** An exception to log. */
	protected Exception exception;
	
	/** A condition which allows the log to be enabled for sending.*/
	private boolean condition = true; 

	/**
	 * Create and logger which is initially enabled.
	 */
	public LogRecord() {
		this.condition = true;
	}

	/**
	 * Create a LogRecord with the specified level and message.
	 * 
	 * @param level   The level of this message.
	 * @param message The message to include.
	 */
	public LogRecord(int level, String message) {
		this();
		this.level = level;
		this.message = message;
		
	}

	/**
	 * Convenience method, add a context entry. If the context is not already in
	 * Existence it is created.
	 */
	public void addContext(String key, String value) {
		if (context == null)
			context = new HashMap();
		context.put(key, value);
	}

	/**
	 * Convenience method, add a tag entry. If the tag-set is not already in
	 * Existence it is created. Null or empty tags are ignored.
	 * @param tag The tag to add.
	 */
	public void addTag(String tag) {
		if (tag == null || tag.equals(""))
			return;
		if (tags == null)
			tags = new ArrayList<String>();
		tags.add(tag);
	}

	/**
	 * Set the tags for this record - overwrites any already set.
	 * 
	 * @param tags
	 */
	public void addTags(List<String> tags) {
		if (tags == null)
			this.tags = tags;
		else
			this.tags.addAll(tags);
	}

	public void clearContext() {
		if (context != null)
			context.clear();
		context = null;
	}

	public void clearTags() {
		if (tags != null)
			tags.clear();
		tags = null;
	}

	/** @return The method which invoked log(). */
	public String getBlock() {
		return block;
	}

	/**
	 * @return the context
	 */
	public Map getContext() {
		return context;
	}

	/** @return The Exception for this record. */
	public Exception getException() {
		return exception;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/** @return The logging level of this record. */
	public int getLevel() {
		return level;
	}

	/** @return The name of the Logger which generated this record. */
	public String getLoggerName() {
		return loggerName;
	}

	/** @return The message from this record. */
	public String getMessage() {
		return message;
	}

	/**
	 * @return The sequence no. (unique to the originating Logger) of this record.
	 */
	public int getSeqno() {
		return seqno;
	}

	/**
	 * @return the severity
	 */
	public int getSeverity() {
		return severity;
	}

	/** @return The name/id of the object which generated this record. */
	public String getSource() {
		return source;
	}

	/**
	 * @return the subSystem
	 */
	public String getSubSystem() {
		return subSystem;
	}

	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * @return the set of tags or null.
	 */
	public List<String> getTags() {
		return tags;
	}

	public String getTagsString() {
		if (tags == null)
			return "";

		StringBuffer sb = new StringBuffer();
		Iterator<String> it = tags.iterator();
		while (it.hasNext()) {
			sb.append("#");
			sb.append(it.next().toLowerCase());
			if (it.hasNext()) // only append comma if not last
				sb.append(",");
		}
		return sb.toString();
	}

	public boolean hasTag(String tag) {
		if (tags == null)
			return false;
		return tags.contains(tag);
	}
	
	/**
	 * @return The thread which was running and invoked the original log() call
	 *         which generated this record.
	 */
	public String getThread() {
		return thread;
	}

	/**
	 * @return The time (millis from 1970) when this record was generated at the
	 *         originating Logger.
	 */
	public long getTime() {
		return time;
	}

	
	
	/**
	 * @return The condition ie whether this record is enabled for sending.
	 */
	public boolean isCondition() {
		return condition;
	}

	/**
	 * Set the name of the method which invoked log().
	 * 
	 * @param method The name of the method.
	 */
	public void setBlock(String method) {
		this.block = block;
	}

	/**
	 * @param context the context to set
	 */
	public void setContext(Map context) {
		this.context = context;
	}

	/**
	 * Set an Exception for this record.
	 * 
	 * @param exception The Exception stored in this record.
	 */
	public void setException(Exception exception) {
		this.exception = exception;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * Set the name of the Logger which generated this record.
	 * 
	 * @param loggerName The name of the Logger which generated this record.
	 */
	public void setLoggerName(String loggerName) {
		this.loggerName = loggerName;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Set the sequence number of this record.
	 * 
	 * @param seqno The (unique to the originating Logger) sequence no. for this
	 *              record.
	 */
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}

	/**
	 * @param severity the severity to set
	 */
	public void setSeverity(int severity) {
		this.severity = severity;
	}

	/** Set the name/id of the object which generated this record. */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @param subSystem the subSystem to set
	 */
	public void setSubSystem(String subSystem) {
		this.subSystem = subSystem;
	}

	/**
	 * @param system the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * Set the tags for this record - overwrites any already set.
	 * 
	 * @param tags
	 */
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	/**
	 * Set the id of the Thread which invoked the original log() call which
	 * generated this record.
	 */
	public void setThread(String thread) {
		this.thread = thread;
	}

	/**
	 * Set the creation time of this record.
	 * 
	 * @param time The time (millis from 1970) when this record was generated at the
	 *             originating Logger.
	 */
	public void setTime(long time) {
		this.time = time;
	}
	
	
	

	/**
	 * @param Set the enablement condition for this record.
	 */
	public void setCondition(boolean condition) {
		this.condition = condition;
	}

	public String toSeverity(int severity) {
		switch (severity) {
		case Logging.SEVERITY_FATAL:
			return "FATAL";
		case Logging.SEVERITY_CRITICAL:
			return "CRITICAL";
		case Logging.SEVERITY_ERROR:
			return "ERROR";
		case Logging.SEVERITY_WARNING:
			return "WARNING";
		case Logging.SEVERITY_INFORMATION:
			return "INFORMATION";
		default:
			return "UNKNOWN";
		}
	}

	public String toString() {
		// return (new Date(time)).toGMTString() + " " + system + "." + subSystem + "."
		// + source + ":" + id + "::" + block
		// + " (" + thread + ") " + toSeverity(severity) + " (" + level + ") "
		// + " " + message + "[" + getTagsString() + "] ";

		return String.format("%tFT%tT %s:%d %s.%s.%s:%s::%s (%s) %12.12s (%3d) %s [%s]\n", time, time, loggerName, seqno, system, subSystem, source,
				(id != null ? id:""), (block != null ? block:""), (thread != null ? thread:"@"), toSeverity(severity), level, message, getTagsString());
		// eg.
		// 2008-01-03 T 15:22:33
		// Transports.Suspension.Cartesian:a2b44-dea32-0ed32bf::preInit (main) ERROR(3)
		// [MODEL,TRACE] Bed Intercept at (12.0,45.23)
		//
	}

}
