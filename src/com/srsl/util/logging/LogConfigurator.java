package com.srsl.util.logging;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.srsl.util.logging.filters.TagLogFilter;
import com.srsl.util.logging.formatters.BasicLogFormatter;
import com.srsl.util.logging.formatters.BogstanLogFormatter;
import com.srsl.util.logging.formatters.CsvLogFormatter;
import com.srsl.util.logging.formatters.DefaultLogFormatter;
import com.srsl.util.logging.formatters.HtmlLogFormatter;
import com.srsl.util.logging.formatters.SmsLogFormatter;
import com.srsl.util.logging.formatters.TabbedLogFormatter;
import com.srsl.util.logging.formatters.XmlLogFormatter;
import com.srsl.util.logging.handlers.ConsoleLogHandler;
import com.srsl.util.logging.handlers.FileLogHandler;
import com.srsl.util.logging.handlers.SmsLogHandler;

public class LogConfigurator {

	/** The stream to read the config from.*/
	InputStream stream;
	
	/** Keeps track of any formatters.*/
	Map<String, LogFormatter> formatters;
	
	/** Keeps track of any handlers.*/
	Map<String, LogHandler> handlers;

	/**
	 * @param stream
	 */
	public LogConfigurator(InputStream stream) {
		this.stream = stream;
	}

	/**
	 * Configure logging using the existing stream.
	 * @throws Exception If anything goes wrong.
	 */
	public void configure() throws Exception {
		BufferedReader streamReader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
		StringBuilder buffer = new StringBuilder();
		String line = null;
		while ((line = streamReader.readLine()) != null) {
			buffer.append(line);
		}

		JSONObject cfg = new JSONObject(buffer.toString());

		JSONArray jformatters = cfg.getJSONArray("formatters");
		formatters = parseFormatters(jformatters);

		JSONArray jhandlers = cfg.getJSONArray("handlers");
		handlers = parseHandlers(jhandlers);

		JSONArray jloggers = cfg.getJSONArray("loggers");
		parseLoggers(jloggers);
		
	}

	private Map<String, LogFormatter> parseFormatters(JSONArray jformatters) throws Exception {
		formatters = new HashMap<String, LogFormatter>();
		for (int i = 0; i < jformatters.length(); i++) {
			JSONObject jfmt = jformatters.getJSONObject(i);
			// {name: "TAG", type: TagLogFormatter},
			String name = jfmt.getString("name");
			String type = jfmt.getString("type"); // convert to real type
			LogFormatter fmt = parseFormatter(type);
			formatters.put(name, fmt);
		}
		return formatters;
	}

	private LogFormatter parseFormatter(String type) throws Exception {
		if (type.equalsIgnoreCase("Basic"))
			return new BasicLogFormatter();
		if (type.equalsIgnoreCase("Bogstan"))
			return new BogstanLogFormatter();
		if (type.equalsIgnoreCase("Csv"))
			return new CsvLogFormatter();
		if (type.equalsIgnoreCase("Default"))
			return new DefaultLogFormatter();
		if (type.equalsIgnoreCase("Html"))
			return new HtmlLogFormatter();
		if (type.equalsIgnoreCase("Tabbed"))
			return new TabbedLogFormatter();
		if (type.equalsIgnoreCase("Xml"))
			return new XmlLogFormatter();
		if (type.equalsIgnoreCase("Sms"))
			return new SmsLogFormatter();

		else
			throw new IllegalArgumentException("Unable to determine formatter type: " + type);
	}

	private Map<String, LogHandler> parseHandlers(JSONArray jhandlers) throws Exception {
		handlers = new HashMap<String, LogHandler>();
		for (int i = 0; i < jhandlers.length(); i++) {
			JSONObject jhandler = jhandlers.getJSONObject(i);
			// {name: "CON", type: Console, level: 4, formatter: TAG
			String name = jhandler.getString("name");
			LogHandler handler = parseHandler(jhandler);
			handlers.put(name,  handler);
		}

		return handlers;
	}

	private LogHandler parseHandler(JSONObject jhandler) throws Exception {

		String type = jhandler.getString("type");
		int level = jhandler.getInt("level");
		String fmt = jhandler.getString("formatter");
		LogFormatter formatter = formatters.get(fmt);

		LogHandler handler = null;
		if (type.equalsIgnoreCase("Console")) {
			handler = new ConsoleLogHandler(formatter);
		} else if (type.equalsIgnoreCase("File")) {
			// {name: "FILE1", type: FileHandler, level: 2, formatter: TAG, file:
			// "c:/users/moi/logs/myweelog", period: "HOUR"}
			String file = jhandler.getString("file");
			String period = jhandler.getString("period");
			int iperiod = 0;
			if (period.equalsIgnoreCase("hourly"))
				iperiod = FileLogHandler.HOURLY_ROTATION;
			else if (period.equalsIgnoreCase("daily")) {
				iperiod = FileLogHandler.DAILY_ROTATION;
			} else if (period.equalsIgnoreCase("weekly")) {
				iperiod = FileLogHandler.WEEKLY_ROTATION;
			}
			handler = new FileLogHandler(file, formatter, iperiod);
		} else if (type.equalsIgnoreCase("SMS")) {
			// {name: "SMS", type: SmsHandler, level: 2, formatter: TXT, to: "+44 1234
			// 567890", subject: "NDM_ALERT"},
			String number = jhandler.getString("number");
			String subject = jhandler.getString("subject");
			handler = new SmsLogHandler(formatter, number, subject);
		} else
			throw new IllegalArgumentException("Unable to determine handler type: " + type);

		handler.setLogLevel(level);
		// inline specified tag filters
		JSONArray jfilters = jhandler.getJSONArray("filters"); // could be null
		LogFilter filter = null;
		if (jfilters != null) {
			filter = new TagLogFilter(true);
			// filters: ["tag1", "tag2", "tag3"...]
			for (int j = 0; j < jfilters.length(); j++) {
				String tag = jfilters.getString(j);
				((TagLogFilter) filter).addTag(tag);
			}

		}
		if (filter != null)
			handler.setFilter(filter);

		return handler;
	}
	
	private void parseLoggers(JSONArray jloggers) throws Exception {
		for (int i = 0; i < jloggers.length(); i++) {
			JSONObject jlogger = jloggers.getJSONObject(i);
			String name = jlogger.getString("name");
			// create the logger, we dont need a ref so no need to save on exit
			Logger logger = Logger.getLogger(name);
			int level = jlogger.getInt("level");
			logger.setLogLevel(level);
			JSONArray jhandlers = jlogger.getJSONArray("handlers");
			for (int j = 0; j < jhandlers.length(); j++) {
				String type = jhandlers.getString(j);
				LogHandler handler = handlers.get(type);
				logger.addHandler(handler);
			}
		}
	}
	
}
