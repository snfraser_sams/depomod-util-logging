package com.srsl.util.logging.filters;

import java.util.ArrayList;
import java.util.List;

import com.srsl.util.logging.LogFilter;
import com.srsl.util.logging.LogRecord;

public class ConjuctCombinerFilter implements LogFilter {

	/** The list of filters to combine.*/
	List<LogFilter> filters;
	
	/**
	 * Create a new ConjuctCombinerFilter with an empty list of filters.
	 */
	public ConjuctCombinerFilter() {
		filters = new ArrayList<LogFilter>();
	}

	/**
	 * Add a filter to the list of filters.
	 * @param filter The filter to add.
	 */
	public void addFilter(LogFilter filter) {
		filters.add(filter);
	}


	/**
	 * Passes a record if ALL the attached filters will pass it.
	 */
	@Override
	public boolean isLoggable(LogRecord record) {
	
		for (LogFilter f : filters) {
			if (!f.isLoggable(record))
				return false;
		}
		return true;
		
	}

}
