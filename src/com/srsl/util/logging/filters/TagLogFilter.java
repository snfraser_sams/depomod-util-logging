package com.srsl.util.logging.filters;

import java.util.ArrayList;
import java.util.List;

import com.srsl.util.logging.LogFilter;
import com.srsl.util.logging.LogRecord;

/**
 * LogFilter which allows log records whose tags match any of the tags in the
 * tag-list. If the flag useSourceClassAsTag is set then this is taken as the
 * ONLY tag for any log records and is checked against the filter's taglist. If
 * not set then the record's actual associated tags are checked. These are
 * mutually exclusive options.
 */
public class TagLogFilter implements LogFilter {

	/**
	 * True if the tag-list allows hierarchic matches. e.g. a record tagged "a.b.c"
	 * would match filters "a.b.c", "a.b" and "a" but not "a.b.d".
	 */
	private boolean hierarchic;

	/**
	 * If true then the source record's classname can be used as its tag - this
	 * option excludes any other tags associated with the record from being checked.
	 */
	private boolean useSourceClassAsTag;

	private List<String> tags;

	/**
	 * Create a TagLogFilter.
	 */
	public TagLogFilter(boolean hierarchic) {
		tags = new ArrayList<String>();
		this.hierarchic = hierarchic;
	}

	/**
	 * Create a (non-hierarchic) tag log filter using the supplied filter tags. Log
	 * records containing any of the supplied tags will be allowed. If hierarchic
	 * then records starting with any of the tags will be allowed.
	 * 
	 * @param tags
	 */
	public TagLogFilter(List<String> tags, boolean hierarchic) {
		this.tags = tags;
		this.hierarchic = hierarchic;
	}

	/**
	 * @param tag The tag to add.
	 * @return True if it was added.
	 * @see java.util.List#add(java.lang.Object)
	 */
	public boolean addTag(String tag) {
		return tags.add(tag);
	}

	@Override
	public boolean isLoggable(LogRecord record) {	
		if (useSourceClassAsTag) {
			String srcTag = record.getSource();
			if (srcTag == null || srcTag.equals(""))
				return false;
			else
				return matchTag(srcTag);
		} else {
			List<String> srcTags = record.getTags();
			// If the source record has no tags associated it will be rejected
			if (srcTags == null || srcTags.size() == 0)
				return false;
			for (String tag : srcTags) {
				if (matchTag(tag))
					return true;
			}
		}
		return false;
	}

	/**
	 * Match a source tag with the internal tag-list tags. If hierarchic we need one
	 * of the tag-list tags to prefix the source tag one of our tags. e.g. source
	 * tag a.b.c starts with our tag a.b Otherwise the source tag must match exactly
	 * to our tag. e.g. a.b.c == a.b.c
	 * 
	 * @param srcTag
	 * @return
	 */
	private boolean matchTag(String srcTag) {
		for (String tag : tags) {
			if (hierarchic) {
				if (srcTag.startsWith(tag))
					return true;
			} else {
				if (srcTag.equalsIgnoreCase(tag))
					return true;
			}
		}
		// no matches so fail
		return false;
	}

	/**
	 * @param useSourceClassAsTag the useSourceClassAsTag to set
	 */
	public void setUseSourceClassAsTag(boolean useSourceClassAsTag) {
		this.useSourceClassAsTag = useSourceClassAsTag;
	}

	
}
