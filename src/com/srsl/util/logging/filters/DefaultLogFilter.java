package com.srsl.util.logging.filters;

import com.srsl.util.logging.LogFilter;
import com.srsl.util.logging.LogRecord;

/** This default implementation of LogFilter allows any 
 *and all records through.*/
public class DefaultLogFilter implements LogFilter {

    public boolean isLoggable(LogRecord record) {
	return true;
    }

}
