package com.srsl.util.logging.handlers;

import java.io.*;

import com.srsl.util.logging.ExtendedLogRecord;
import com.srsl.util.logging.LogFormatter;
import com.srsl.util.logging.LogHandler;
import com.srsl.util.logging.LogRecord;

public class ConsoleLogHandler extends LogHandler {

	/** Create a ConsoleLogHandler using the specified formatter. */
	public ConsoleLogHandler(LogFormatter formatter) {
		super(formatter);
		System.err.println(formatter.getHead());
	}

	/** Publish a LogRecord to System.err . */
	public void publish(LogRecord record) {
		System.err.print(formatter.format(record));
	}

	public boolean isLoggable(ExtendedLogRecord record) {
		return (record.getLevel() <= logLevel);
	}

	/** Write the tail. */
	public void close() {
		System.err.println(formatter.getTail());
	}

}
