package com.srsl.util.logging.handlers;

import com.srsl.util.logging.LogFormatter;
import com.srsl.util.logging.LogHandler;
import com.srsl.util.logging.LogRecord;

public class SmsLogHandler extends LogHandler  {

	/** The number to send to. Like:  +nn nnnn nnnn  spaces ignored */
	String number;
	
	/** A subject to include in the message.*/
	String subject;
	
	/**
	 * @param formatter
	 */
	public SmsLogHandler(LogFormatter formatter, String number, String subject) {
		super(formatter);
		this.number = number;
		this.subject = subject;
	}

	@Override
	public void publish(LogRecord record) {
		// generate an email using the log record and previously supplied parameters
		String message = formatter.format(record);
		// send by text service - somehow...needs some login params?? eg account, authtoken?
	}

	@Override
	public void close() {
		//out.println(formatter.getTail());
		//out.close();
	}

	
}
