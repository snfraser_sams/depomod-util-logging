package com.srsl.util.logging.handlers;

import java.io.*;
import java.util.*;

import com.srsl.util.logging.LogHandler;
import com.srsl.util.logging.LogRecord;
import com.srsl.util.logging.formatters.BasicLogFormatter;

import java.text.*;
import java.net.*;

/** Class which implements sending of log messages to a syslog receiver.
 * see: https://tools.ietf.org/html/rfc5426 and https://tools.ietf.org/html/rfc5424
 
 */
public class SyslogHandler extends LogHandler {

	/** Standard port for receiving syslog messages.*/
	public static int SYSLOG_PORT = 514;

	/** Basic syslog style date format for records.*/
	public SimpleDateFormat sysf = new SimpleDateFormat("MMM dd HH:mm:ss");

	/** A datagram socket.*/
	private DatagramSocket socket;

	/** Identity of the host to receive the messages.*/
	private String logHost;

	/*8 The syslog port.*/
	private int logPort = SYSLOG_PORT;

	/** This (sending) machine.*/
	private String host;

	// counts errors.
	private int errc = 0;

	/**
	 * Create a new SyslogHandler.
	 * 
	 * @throws Exception if there is a problem binding.
	 */
	public SyslogHandler(String logHost, int logPort) throws Exception {

		super(new BasicLogFormatter());
		// bind anywhere available.
		socket = new DatagramSocket();

		this.logHost = logHost;
		this.logPort = logPort;

		host = InetAddress.getLocalHost().getHostName();

	}

	/** Overridden to write a formatted LogRecord to syslog via UDP. 
	 * Currently this assumes a local0 level for all messages (14).*/
	public void publish(LogRecord record) {

		// interpret the pri and tag fields from record content..
		int pri = 16; // assume local0.level messages for now
		String tag = record.getSource();

		String message = "<" + pri + ">" + sysf.format(new Date()) + " " + host + " " + tag + ": "
				+ formatter.format(record);

		System.err.println("Syslog Message: [" + message + "]");

		byte[] buffer = new byte[message.length()];

		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream(buffer.length);

			DataOutputStream dos = new DataOutputStream(baos);

			dos.writeBytes(message);
			buffer = baos.toByteArray();

			InetAddress address = InetAddress.getByName(logHost);

			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, logPort);

			socket.send(packet);
		} catch (Exception e) {
			errc++;
			if (errc < 10)
				e.printStackTrace();
		}

	}

	public void close() {

		try {
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
