/**
 * 
 */
package com.srsl.util.logging.handlers;

import java.util.Properties;

import com.srsl.util.logging.LogFormatter;
import com.srsl.util.logging.LogHandler;
import com.srsl.util.logging.LogRecord;
import com.srsl.util.logging.util.CloudLogClient;

/**
 * A LogHandler which sends log info and environment info to a dedicated cloud server.
 * No assumptions are made about the server other than that it gives out a session uuid
 * and receives environment and log info from clients by some transport mechanism.
 * Typically the communication will be by HTTP but this is not mandated.
 * 
 * The log handler is created with a concrete implementation of a client which should 
 * implement the protocol expected by the server.
 * 
 * @author SA05SF
 *
 */
public class CloudLogHandler extends LogHandler {

	/** Handles connection with the cloud server. */
	private CloudLogClient xfer;

	/** A session uuid obtained from the server when we first connect. */
	String sessionUuid = null;

	/**
	 * Setup a Cloud based LogHandler using supplied auth details and local env.
	 * Connect to the cloud log server and retrieve a session uuid.
	 * @param xfer    A concrete implementation of a cloud log transfer client.
	 * @param auth    Placeholder for more complex auth info eg tokens/keys.
	 * @param userenv User/host/sw environment.
	 * @throws Exception if anything goes wrong with the connection 
	 * - if this occurs, no session uuid will be received and little point in continuing.
	 */
	public CloudLogHandler(CloudLogClient xfer, String auth, Properties userenv) throws Exception {
		super(null);
		this.xfer = xfer;

		// collect environment and software details
		//Properties env = new Properties();
		// e.g. host, user, os, java-version etc

		// e.g. sw-version/build, license-id, uid etc

		// TODO push ident to server and receive a session uuid.
		sessionUuid = xfer.connect(auth, userenv);
	}

	@Override
	/**
	 * Publish a log record to the cloud log server.
	 * @param record The record to publish.
	 */
	public void publish(LogRecord record) {
		// TODO send log info to server

		// construct a payload - jsonize the logrecord info
		// send to server via http(s)
		try {
			xfer.send(record);
		} catch (Exception e) {
			// not much we can do here...
		}

	}

	@Override
	/**
	 * Close the connection to the cloud log server.
	 */
	public void close() {
		// TODO close session at server
		try {
			xfer.close();
		} catch (Exception e) {
			// not much we can do here...
		}

	}

}
