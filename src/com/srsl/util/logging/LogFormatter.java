package com.srsl.util.logging;

import java.text.*;
import java.util.*;

/**
 * Interface to be implemented by any class which is required to format Log
 * messages.
 */
public abstract class LogFormatter {

	/** Standard date format ISO 8601. */
	public static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

	/** Standard UTC timezone for date formatter.*/
	public static SimpleTimeZone UTC = new SimpleTimeZone(0, "UTC");

	/** The Date format in use (defaults to SDF). */
	protected SimpleDateFormat df;

	public LogFormatter() {
		df = SDF;
		df.setTimeZone(UTC);
	}

	/** Format a LogRecord. */
	public abstract String format(LogRecord record);

	/** Return the head sequence for the formatter. */
	public abstract String getHead();

	/** Return the Tail sequence for the formatter. */
	public abstract String getTail();

	/** Return the file name extension for the formatter. */
	public abstract String getExtensionName();

	/**
	 * Override the default DateFormat to use for date and time formatting of output.
	 * 
	 * @param sdf The DateFormat to use.
	 */
	public void setDateFormat(SimpleDateFormat df) {
		this.df = df;
	}

}
