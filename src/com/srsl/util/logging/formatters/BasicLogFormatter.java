package com.srsl.util.logging.formatters;

import java.util.*;

import com.srsl.util.logging.LogFormatter;
import com.srsl.util.logging.LogRecord;


/**
 * This '<i>Basic</i>' LogFormatter just prints out the time, source and
 * message.
 */
public class BasicLogFormatter extends LogFormatter {

	/**
	 * Default point where log messages are broken over a line. Any messages under
	 * this length are written to same line as time. Any messages over this length
	 * are started on a new line.
	 */
	public static final int DEFAULT_BREAK_LENGTH = 120;

	/** Point where messages are broken over a line. */
	protected int maxLength;

	/** Create a BasicLogFormatter with default break length. */
	public BasicLogFormatter() {
		this(DEFAULT_BREAK_LENGTH);
	}

	/** Create a BasicLogFormatter with specified break length. */
	public BasicLogFormatter(int maxLength) {
		super();
		this.maxLength = maxLength;
	}

	public String format(LogRecord record) {
		long time = record.getTime();
		return String.format("%tFT%tT [%12.12s] %s :: %." + maxLength + "s  %s\n", time, time, record.toSeverity(record.getSeverity()), record.getSource(), record.getMessage(), getTagsString(record));

	}

	public String getHead() {
		return "Basic-Logging START--->";
	}

	public String getTail() {
		return "<---END Basic-Logging.";
	}

	/** Return the file name extension for the formatter. */
	public String getExtensionName() {
		return "txt";
	}

	private String getTagsString(LogRecord record) {
		List<String> tags = record.getTags();
		if (tags == null)
			return "";

		StringBuffer sb = new StringBuffer();
		Iterator<String> it = tags.iterator();
		while (it.hasNext()) {
			sb.append("#");
			sb.append(it.next().toLowerCase());
			if (it.hasNext()) // only append comma if not last
				sb.append(" ");
			
		}
		return sb.toString();
	}

}
