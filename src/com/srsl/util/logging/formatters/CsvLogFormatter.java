package com.srsl.util.logging.formatters;

import java.util.*;

import com.srsl.util.logging.LogFormatter;
import com.srsl.util.logging.LogRecord;

import java.text.*;

public class CsvLogFormatter extends LogFormatter {

    final String DEFAULT_SEPERATOR = ",";

    String seperator = DEFAULT_SEPERATOR;

    public CsvLogFormatter() {
	super();
    } 

    public String format(LogRecord record) {
	String result = 
	    record.getSeqno()+seperator+
	    record.getLoggerName()+seperator+
	    record.getLevel()+seperator+
	    df.format(new Date(record.getTime()))+seperator+
	    record.getSystem()+seperator+
	    record.getSubSystem()+seperator+
	    record.getSource()+seperator+
	    record.getBlock()+seperator+
	    record.getThread()+seperator+
	    record.getMessage();
	
	if (record.getException() != null) {
	    result += seperator+record.getException();
	}
	return result;
    }
    
    public String getHead() {
	return "CSV Logging START---";
    }
    
    public String getTail() {
	return "---END CSV Logging.";
    }
   
    /** Return the file name extension for the formatter.*/
    public String getExtensionName() { return "csv"; }

    /** Set the separator character(s).*/
    public void setSeparator(String seperator) {
	this.seperator = seperator;
    }

}
