package com.srsl.util.logging.formatters;

import java.util.*;

import com.srsl.util.logging.LogFormatter;
import com.srsl.util.logging.LogRecord;

import java.text.*;

/** This '<i>Bog-Standard</i>' LogFormatter just prints out the time, source and message.
 */
public class BogstanLogFormatter extends LogFormatter {

    public boolean showLogName = false;

    public BogstanLogFormatter() {
	super();
    }

    public String format(LogRecord record) {	

    	long time = record.getTime();
    	
    	return String.format("%tFT%tT %s.%s.%s:: %s \n", 
    			time,time,
    			  record.getSystem(),
    			  record.getSubSystem(),
    			  record.getSource(),
    			  record.getMessage()
    			);
	
    }
    
    public String getHead() {
	return "Logging START--->";
    }
    
    public String getTail() {
	return "<---END Logging.";
    }

    /** Return the file name extension for the formatter.*/
    public String getExtensionName() { return "txt"; }

}
