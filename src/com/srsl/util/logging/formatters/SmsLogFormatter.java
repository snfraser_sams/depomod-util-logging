package com.srsl.util.logging.formatters;

import com.srsl.util.logging.LogFormatter;
import com.srsl.util.logging.LogRecord;

public class SmsLogFormatter extends LogFormatter {

	@Override
	public String format(LogRecord record) {
		// turn the record into a SMS text
		
		// 2022-01-13T03:34:56 SYS:SUB:SRC::A SHORT MESSAGE TO YOU
		
		return String.format("%tFT%tT %s:%s:%s:: %s \n", 
    			record.getTime(), record.getTime(),
    			  record.getSystem().toUpperCase(),
    			  record.getSubSystem().toUpperCase(),
    			  record.getSource().toUpperCase(),
    			  record.getMessage().toUpperCase()
    			);
	}

	@Override
	public String getHead() {
		return "SMS Logging START---";
	}

	@Override
	public String getTail() {
		return "---END SMS Logging.";
	}

	@Override
	public String getExtensionName() {
		return "sms";
	}

}
