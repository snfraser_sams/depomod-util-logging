package com.srsl.util.logging.formatters;

import java.util.Iterator;

import com.srsl.util.logging.LogFormatter;
import com.srsl.util.logging.LogRecord;

public class DefaultLogFormatter extends LogFormatter {

	@Override
	public String format(LogRecord record) {
		// return record.toString();
		String id = record.getId();
		String block = record.getBlock();
		String thread = record.getThread();
		return String.format("%tFT%tT %s:%d %s.%s.%s:%s::%s (%s) %-11.11s (%1d) %s [%s]\n", 
				record.getTime(),
				record.getTime(), 
				record.getLoggerName(), 
				record.getSeqno(), 
				record.getSystem(), 
				record.getSubSystem(),
				record.getSource(), 
				(id != null ? id : ""), 
				(block != null ? block : ""),
				(thread != null ? "@"+thread : "@?"), 
				record.toSeverity(record.getSeverity()), 
				record.getLevel(),
				record.getMessage(), 
				getTagsString(record));

	}

	private String getTagsString(LogRecord record) {
		if (record.getTags() == null)
			return "";

		StringBuffer sb = new StringBuffer();
		Iterator<String> it = record.getTags().iterator();
		while (it.hasNext()) {
			sb.append("#");
			sb.append(it.next().toLowerCase());
			if (it.hasNext()) // only append comma if not last
				sb.append(",");
		}
		return sb.toString();
	}

	@Override
	public String getHead() {
		return "START default logger";
	}

	@Override
	public String getTail() {
		return "END default logger";
	}

	@Override
	public String getExtensionName() {
		return ".txt";
	}

}
