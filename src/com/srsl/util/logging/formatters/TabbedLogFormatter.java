/**
 * 
 */
package com.srsl.util.logging.formatters;

import com.srsl.util.logging.LogFormatter;
import com.srsl.util.logging.LogRecord;

/**
 * A formatter which indents the output according to the record level. The size
 * and character used for tabs can be set for each formatter instance. As a rule
 * users should refrain from using different tab char and tab size for different
 * formatters directed to the same handler or output stream as this will likely
 * cause confusing output.
 * 
 * @author SA05SF
 *
 */
public class TabbedLogFormatter extends LogFormatter {

	private static final char DEFAULT_TAB_CHAR = ' ';

	private char tabChar = DEFAULT_TAB_CHAR;

	/** The tabs indents preformed - allow upto 10 levels, though this is excessively deep.*/
	private String[] tabs = new String[10];

	private static final int DEFAULT_NTABS = 5;

	private int ntabs = DEFAULT_NTABS;

	/** Set true if timestamp should be prefix - otherwise is postfix. */
	private boolean prefixTime = false;

	
	
	/**
	 * 
	 */
	public TabbedLogFormatter() {
		this(DEFAULT_NTABS, DEFAULT_TAB_CHAR);
	}

	/**
	 * @param tabChar
	 */
	public TabbedLogFormatter(int ntabs, char tabChar) {
		super();
		// by default we setup as 5 spaces
		setupTabs(ntabs, tabChar);
	}

	/** Setup the default tabs once and for all. */
	public void setupTabs(int antab, char atabChar) {

		ntabs = antab;
		tabChar = atabChar;

		// technically level 0 does not exist so we make it a 1
		// we assume zero padding for level 0 and 1 and n-1 for all others

		tabs[0] = "";
		tabs[1] = "";

		// fill in the remaining tab indents - 10 levels is huge but allowed
		for (int i = 2; i < 10; i++) {
			StringBuffer atab = new StringBuffer(" ");
			int ni = (i <= 1 ? 0 : i - 1);
			for (int j = 0; j < ni; j++) {
				for (int k = 0; k < ntabs; k++) {
					atab.append(tabChar);
				}
			}
			tabs[i] = atab.toString();
		}
	}

	@Override
	public String format(LogRecord record) {

		int level = record.getLevel();
		if (level >= 10)
			level = 10;
		int nt = 5 * level;

		long time = record.getTime();

		String tabString = "";
		if (tabChar != DEFAULT_TAB_CHAR) {
			tabString = tabs[record.getLevel()];
			if (prefixTime)
				return String.format("%tFT%tT %s %s.%s.%s::%s %s\n", time, time, tabString, record.getSystem(),
						record.getSubSystem(), record.getSource(), record.getId(), record.getMessage());
			else
				return String.format("%s %s.%s.%s::%s %s @ %tFT%tT\n", tabString, record.getSystem(),
						record.getSubSystem(), record.getSource(), record.getId(), record.getMessage(), time, time);
		} else {
			String tabFmt = "%" + nt + "." + nt + "s ";
			if (prefixTime)
				return String.format("%tFT%tT " + tabFmt + "%s.%s.%s::%s %s\n", time, time, "", record.getSystem(),
						record.getSubSystem(), record.getSource(), record.getId(), record.getMessage());
			else
				return String.format(tabFmt + " %s.%s.%s::%s %s @ %tFT%tT\n", "", record.getSystem(),
						record.getSubSystem(), record.getSource(), record.getId(), record.getMessage(), time, time);

		}

	}

	@Override
	public String getHead() {
		return "Logging START--->";
	}

	@Override
	public String getTail() {
		return "<---END Logging.";
	}

	@Override
	public String getExtensionName() {
		return ".txt";
	}

	/**
	 * @param prefixTime the prefixTime to set
	 */
	public void setPrefixTime(boolean prefixTime) {
		this.prefixTime = prefixTime;
	}

}
