/**
 * 
 */
package com.srsl.util.logging.util;

import java.util.Properties;

import com.srsl.util.logging.LogRecord;

/**
 * 
 * @author SA05SF
 *
 */
public interface CloudLogClient {

	/**
	 * Connect to the cloud server.
	 * @param auth Authentication information - TBD.
	 * @param env Client environment info - host, software etc - TBD.
	 * @return A session uuid - TBD.
	 * @throws Exception If anything goes wrong.
	 */
	public String connect(String auth, Properties env) throws Exception;
	
	/** 
	 * Send a log-record to the cloud server.
	 * @param record A log-record.
	 * @throws Exception If anything goes wrong.
	 */
	public void send(LogRecord record) throws Exception;
	
	/** 
	 * Close the connection to the cloud server.
	 * @throws Exception If anything goes wrong.
	 */
	public void close() throws Exception;
	
}
