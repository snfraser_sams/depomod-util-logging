package com.srsl.util.logging;

import java.io.*;
import java.util.*;
import javax.swing.event.*;

/**
 * Global Logging manager. Holds configuration information for all Loggers and
 * can retrieve references to any named Loggers.
 */
public class LogManager implements Logging {

	protected static Map registry;

	protected static EventListenerList listenerList;

	protected static LogManagerEvent logManagerEvent;

	static {
		registry = Collections.synchronizedMap(new HashMap());
		listenerList = new EventListenerList();
	}

	public static void configure(InputStream stream) throws Exception {
		LogConfigurator cfg = new LogConfigurator(stream);
		cfg.configure();
	}
	
	/**
	 * If a Logger with the specified name exists (is registered) this is returned,
	 * otherwise a Logger with this name is created (with default settings),
	 * registered and returned. If the name is null or empty, the Anonymous Logger
	 * ("ANON") is returned.
	 * 
	 * @param name The Logger's name.
	 * @return An existing or new Logger.
	 */
	public static Logger getLogger(String name) {
		
		if (registry.containsKey(name))
			return (Logger) registry.get(name);
		Logger logger = new Logger(name);
		registry.put(name, logger);
		fireLoggerAdded(logger);
		return logger;
	}

	/**
	 * If a Logger with the name of the specified Class exists (is registered) this
	 * is returned, otherwise a Logger with this name is created (with default
	 * settings), registered and returned.
	 * 
	 * @param clazz The Class whose fully qualified name is used for the Logger's
	 *              name.
	 * @return An existing or new Logger.
	 */
	public static Logger getLogger(Class clazz) {
		return getLogger(clazz.getName());
	}

	

	/**
	 * Returns an Iterator over the Loggers managed by the LogManager.
	 * 
	 * @return An Iterator over the managed Loggers.
	 */
	public static Iterator listLoggers() {
		return registry.values().iterator();
	}

	


	/**
	 * Configure all Loggers from a config file. For now this just sets the
	 * logLevels at startup - others will be added.
	 * 
	 * @param Properties The set of config Properties, previously loaded from a
	 *                   file.
	 */
	public static void configureLoggers(Properties config) {
		Iterator it = registry.values().iterator();
		while (it.hasNext()) {
			Logger logger = (Logger) it.next();
			logger.configure(config);
		}
	}

	public static void addLogManagerListener(LogManagerListener l) {
		listenerList.add(LogManagerListener.class, l);
	}

	public static void removeLogManagerListener(LogManagerListener l) {
		listenerList.remove(LogManagerListener.class, l);
	}

	// Notify all listeners that have registered interest for
	// notification on this event type. The event instance
	// is lazily created using the parameters passed into
	// the fire method.

	protected static void fireLoggerAdded(Logger logger) {
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == LogManagerListener.class) {
				// Lazily create the event:
				if (logManagerEvent == null)
					logManagerEvent = new LogManagerEvent(LogManager.class);
				System.err.println("LM: lev. do setlogger: " + logger);
				logManagerEvent.setLogger(logger);
				System.err.println("LM: lev. done setlogger: " + logger);
				((LogManagerListener) listeners[i + 1]).loggerAdded(logManagerEvent);
			}
		}
	}

	protected static void fireLoggerChanged(Logger logger) {
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == LogManagerListener.class) {
				// Lazily create the event:
				if (logManagerEvent == null)
					logManagerEvent = new LogManagerEvent(LogManager.class);
				logManagerEvent.setLogger(logger);
				((LogManagerListener) listeners[i + 1]).loggerChanged(logManagerEvent);
			}
		}
	}

}
